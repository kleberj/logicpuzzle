The logicpuzzle bundle allows you to typeset various logic
puzzles. At the moment the following puzzles are supported:

* 2D-Sudoku (aka Magiequadrat, Diagon, ...)
* Battleship (aka Bimaru, Marinespiel, Batalla Naval, ...)
* Bokkusu (aka Kakurasu, Feldersummenrätsel, ...)
* Bridges (aka Brückenbau, Hashi, ...)
* Chaos Sudoku
* Four Winds (aka Eminent Domain, Lichtstrahl, ...)
* Hakyuu (aka Seismic, Ripple Effect, ...)
* Hitori
* Kakuro
* Kendoku (aka Mathdoku, Calcudoku, Basic, MiniPlu, Ken Ken,
               Square Wisdom, Sukendo, Caldoku, ...)
* Killer Sudoku (aka Samunapure, Sum Number Place, Sumdoku,
                     Gebietssummen, ...)
* Laser Beam (aka Laserstrahl, ...)
* Magic Labyrinth (aka Magic Spiral, Magisches Labyrinth, ...)
* Magnets (aka Magnetplatte, Magnetfeld, ...)
* Masyu (aka Mashi, {White|Black} Pearls, ...)
* Minesweeper (aka Minensuche, ...)
* Nonogram (aka Griddlers, Hanjie, Tsunami, Logic Art,
                Logimage, ...)
* Number Link (aka Alphabet Link, Arukone, Buchstabenbund, ...)
* Resuko
* Schatzsuche
* Skyline (aka Skycrapers, Wolkenkratzer, Hochhäuser, ...)
  incl. Skyline Sudoku and Skyline Sudoku (N*N) variants
* Slitherlink (aka Fences, Number Line, Dotty Dilemma,
               Sli-Lin, Takegaki, Great Wall of China,
               Loop the Loop, Rundweg, Gartenzaun, ...)
* Star Battle (aka Sternenschlacht, ...)
* Stars and Arrows (aka Sternenhimmel, ...)
* Sudoku
* Sun and Moon (aka Sternenhaufen, Munraito, ...)
* Tents and Trees (aka Zeltlager, Zeltplatz, Camping, ...)
* Tunnel

License: LPPL

Changes in v2.5:

*   added support for Nonogram puzzle

    [https://bitbucket.org/kleberj/logicpuzzle/wiki/Nonogram](https://bitbucket.org/kleberj/logicpuzzle/wiki/Nonogram)

    [https://bitbucket.org/kleberj/logicpuzzle/downloads/example-nonogram.pdf](https://bitbucket.org/kleberj/logicpuzzle/downloads/example-nonogram.pdf)

    feature request by: Theresa Spannbauer

*   added documented source (dtx)

*   added LPpreset TikZ style to all puzzle environments plus some minor
  changes